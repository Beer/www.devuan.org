# Devuan Web Source

This is the source for Devuan's web site `www.devuan.org`, and its
fore-runner `beta.devuan.org`.

Development occurs on the `new-beta` branch, where only the `source/`
directory is used and automatically published verbatim to
`beta.devuan.org`. The main site `www.devuan.org` is published by
manual copy of `beta.devuan.org` on the server, except for
`sitemap.xml` which is generated with respect to
`https://beta.devuan.org`.

The file `.PUBLISH` is a control file for publishing the current
`beta.devuan.org` onto `www.devuan.org`. By touching that file, for
example adding a who+when+why note to it, and pushing that, the
automatic publishing process will also publish `www.devuan.org`
verbatim from `beta.devuan.org`, except for `sitemap.xml` which is
generated with respect to `https://www.devuan.org`.

All else in this project is old stuff.

## License

The source code is free software distributed under the GNU Affero General
Public License, version 3, or, at your option, any later version. (See
[COPYING](./COPYING).)

The website contents are creative commons released under
CC-BY-ND-4.0-International.

## Editing Pages

Thank you for contributing!
