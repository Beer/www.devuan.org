server {
	listen 80;
#	listen [::]:80;

  	server_name pkginfo.devuan.org;
	
	location ~ ^/.well-known/acme-challenge {
	    root /home/pkginfo.devuan.org/letsencrypt;
  	}

	location / {	
		return 301 https://$server_name$request_uri;
	}


}

server {

	listen 443 ssl;
#	listen [::]:443;

  	server_name pkginfo.devuan.org;
	root /home/pkginfo.devuan.org/public;
	


	location / {
		autoindex off;
	}

        location /cgi-bin/ {
                gzip off;
                autoindex off;
                #root  /home/info.devuan.org/cgi-bin;
                # Fastcgi socket
                fastcgi_pass  unix:/var/run/fcgiwrap.socket;
                # Fastcgi parameters, include the standard ones
                include /etc/nginx/fastcgi_params;
                fastcgi_param SCRIPT_FILENAME  $document_root$fastcgi_script_name;
                fastcgi_param SCRIPT_NAME $fastcgi_script_name; 
        }

	ssl on;

	ssl_certificate /etc/letsencrypt/live/pkginfo.devuan.org/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/pkginfo.devuan.org/privkey.pem;
	ssl_protocols          TLSv1.2 TLSv1.1 TLSv1;
	ssl_ciphers            "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
	ssl_ecdh_curve secp384r1; 
	ssl_prefer_server_ciphers on;
	ssl_session_cache   builtin:1000 shared:SSL:10m;
	resolver 213.186.33.99 valid=300s;
	resolver_timeout 3s;
}
