Anton Zinoviev <zinoviev@debian.org>
Daniel Reurich <daniel@centurion.net.nz>
Denis Roio <jaromil@dyne.org>
Devuan Hebrew Team <debian-hebrew-common@lists.alioth.debian.org>
Devuan Libvirt Maintainers <pkg-libvirt-maintainers@devuan.org>
Dima Krasner <dima@dimakrasner.com>
Dimitri Puzin <max@psycast.de>
Eddy Petrişor <eddy.petrisor@gmail.com>
Eugeniy Meshcheryakov <eugen@debian.org>
Franco (nextime) Lanza <nextime@devuan.org>
Frits Daalmans <frits@daalmansdata.eu>
Jordi Mallach <jordi@debian.org>
Jude Nelson <judecn@devuan.org>
Kenshi Muto <kmuto@debian.org>
Kęstutis Biliūnas <kebil@kaunas.init.lt>
Konstantinos Margaritis <markos@debian.org>
Otavio Salvador <otavio@debian.org>
Per Olofsson <pelle@debian.org>
Peter Novodvorsky <nidd@debian.org>
Petter Reinholdtsen <pere@debian.org>
Recai Oktas <roktas@omu.edu.tr>
Roger Leigh <rleigh@debian.org>
Safir Secerovic <sapphire@linux.org.ba>
Timur Birsh <taem@linukz.org>
Y Giridhar Appaji Nag <appaji@debian.org>
